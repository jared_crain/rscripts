##RScript for calling MSTMap with Family 5 Genotype Matrix
##Jared Crain
##September 19, 2014
##jcrain@ksu.edu

source("/homes/jcrain/R/RScripts/HapFileFunctionsGraphingAndManipulation.R")

#Import family5filtered genotype matrxi
fam5="/homes/jcrain/DataAnalysis/CIMMYTRIL/Family5/Family5Filtered_genotypematrix.txt"
fam5=read.table(fam5, header=TRUE)

#convert to binary matrix
fam5bin=geno_to_binary(fam5)

##Function to change 1, -1, 0 to MSTMap format
binary_to_AB=function(x="binary geno matrix"){
  y=x[,12:ncol(x)]
  xprime=x[,1:11]
  #IF you want to change to A and B, use this after first converting to a 1, -1 matrix
  y[y==1]="A"
  y[y==-1]="B"
  y[y==0]="X"
  y[is.na(y)]="U"
  return(cbind(xprime, y))}

#Run function
map5=binary_to_AB(fam5bin)

#Delte columns not needed in MSTMap
map5=map5[,c(2, 12:ncol(map5))]

#Transpose matrix so it is formatted for MSTMap
map5=t(map5)

#Add colnames
colnames(map5)=map5[1,]
map5=map5[-1,]

#write map file
write.table(map5, file="Family5MapIn.txt", row.names=TRUE,col.names=NA , quote=FALSE,sep="\t")
